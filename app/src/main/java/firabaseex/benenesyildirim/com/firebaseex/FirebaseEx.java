package firabaseex.benenesyildirim.com.firebaseex;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.ShareEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import io.fabric.sdk.android.Fabric;

public class FirebaseEx extends AppCompatActivity implements View.OnClickListener {

    private TextView descTv, appTitleTV,tokenTv;
    private EditText nameEt, emailEt;
    private Button okBtn, loadBtn, shareBtn;
    private DatabaseReference databaseReference;
    private String userID, token;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SharedPreferences sharedPreferences;
    private static final String userPreferences = "preferences";
    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private String userKey;
    private FirebaseRemoteConfig remoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_firebase_ex);

        fabricLoginEventListener();
        remoteConfig();
        initFireBase();
        init();
        controlUser();

        String token = sharedPreferences.getString("token", "");
        tokenTv = findViewById(R.id.tokenTv);
        tokenTv.setText(token);
    }

    private void fabricLoginEventListener() {
        Answers.getInstance().logLogin(new LoginEvent()
                .putMethod("Login")
                .putSuccess(true));
    }

    private void remoteConfig() {
        remoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG).build();
        remoteConfig.setConfigSettings(configSettings);

        remoteConfig.setDefaults(R.xml.remote_config);

        long cacheTiming = 30;
        remoteConfig.fetch(cacheTiming).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(FirebaseEx.this, "Fetch Succeeded!", Toast.LENGTH_SHORT).show();
                    remoteConfig.activateFetched();
                } else {
                    Toast.makeText(FirebaseEx.this, "Fetch Failed!", Toast.LENGTH_SHORT).show();
                }
                displayWelcomeMessage();
            }
        });
    }

    private void displayWelcomeMessage() {
        String welcomeMessage = remoteConfig.getString("app_title");
        appTitleTV.setText(welcomeMessage);
    }

    private void initFireBase() {
        databaseReference = firebaseDatabase.getReference("users");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        FirebaseApp.initializeApp(getApplicationContext());
    }

    private void controlUser() {
        sharedPreferences = getSharedPreferences(userPreferences, MODE_PRIVATE);
        userKey = sharedPreferences.getString("key", "");
        if (!userKey.isEmpty()) {
            DatabaseReference user = firebaseDatabase.getReference("users").child(userKey);
            user.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        nameEt.setVisibility(View.GONE);
                        emailEt.setVisibility(View.GONE);
                        UserProperties value = dataSnapshot.getValue(UserProperties.class);
                        descTv.setText("Name: " + value.getName() + "\nEmail: " + value.getEmail());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void init() {
        descTv = findViewById(R.id.descTV);
        appTitleTV = findViewById(R.id.appTitle);
        nameEt = findViewById(R.id.nameET);
        emailEt = findViewById(R.id.emailET);

        okBtn = findViewById(R.id.enterBTN);
        okBtn.setOnClickListener(this);

        loadBtn = findViewById(R.id.loadBtn);
        loadBtn.setOnClickListener(this);

        shareBtn = findViewById(R.id.shareBTN);
        shareBtn.setOnClickListener(this);
    }

    private void createUser(String name, String email,String token) {
        if (TextUtils.isEmpty(userID)) {
            userID = databaseReference.push().getKey();
            token = FirebaseInstanceId.getInstance().getToken();
            UserProperties user = new UserProperties(name, email, token);
            databaseReference.child(userID).setValue(user);
            Toast.makeText(getApplicationContext(), token, Toast.LENGTH_SHORT).show();

            fabricSignUpEventListener(name, email);

            SharedPreferences.Editor editor = getSharedPreferences(userPreferences, MODE_PRIVATE).edit();
            editor.putString("key", userID);
            editor.apply();
            addUserChangeListener(false);
        }
    }

    private void fabricSignUpEventListener(String name, String email) {
        Answers.getInstance().logSignUp(new SignUpEvent()
                .putMethod("user")
                .putSuccess(true)
                .putCustomAttribute("name", name)
                .putCustomAttribute("email", email));
    }

    private void addUserChangeListener(boolean isEdit) {
        sharedPreferences = getSharedPreferences(userPreferences, MODE_PRIVATE);
        userKey = sharedPreferences.getString("key", "");
        if (!userKey.isEmpty()) {
            userID = userKey;
        }

        if (isEdit) {
            UserProperties user = new UserProperties(nameEt.getText().toString(), emailEt.getText().toString(),  FirebaseInstanceId.getInstance().getToken());
            databaseReference.child(userID).setValue(user);
        }

        databaseReference.child(userID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserProperties user = dataSnapshot.getValue(UserProperties.class);
                if (user == null) {
                    Toast.makeText(getApplicationContext(), "User Data is empty!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "User is changed!", Toast.LENGTH_SHORT).show();
                descTv.setText(user.getName() + ", " + user.getEmail());
                emailEt.setText("");
                nameEt.setText("");

            }

            @Override
            public void onCancelled(DatabaseError error) {
                Toast.makeText(getApplicationContext(), "Cant read the User", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == okBtn.getId()) {
            createOrSalud();
        }
        if (view.getId() == loadBtn.getId()) {
            editUser();
        }
        if (view.getId() == shareBtn.getId()) {
            shareUser();
        }
    }

    private void shareUser() {
        if (!descTv.getText().toString().isEmpty()) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            shareIntent.putExtra(Intent.EXTRA_TEXT, descTv.getText().toString());
            startActivity(Intent.createChooser(shareIntent, "Share Via"));

            Answers.getInstance().logShare(new ShareEvent());
        } else {
            Toast.makeText(getApplicationContext(), "User not Found!", Toast.LENGTH_SHORT).show();
        }
    }

    public void createOrSalud() {
        Bundle bundle = new Bundle();
        bundle.putString("ButtonClick", "Clicked!");
        mFirebaseAnalytics.logEvent("BtnClicked", bundle);
        String name = nameEt.getText().toString();
        String email = emailEt.getText().toString();

        if (TextUtils.isEmpty(userKey)) {
            createUser(name, email,token);
        } else {
            nameEt.setVisibility(View.VISIBLE);
            emailEt.setVisibility(View.VISIBLE);
            okBtn.setVisibility(View.GONE);
            loadBtn.setVisibility(View.VISIBLE);
        }
    }

    public void editUser() {
        if (nameEt.getVisibility() == View.VISIBLE && emailEt.getVisibility() == View.VISIBLE) {
            if (!nameEt.getText().toString().isEmpty() && isValidEmail(emailEt.getText().toString())) {
                addUserChangeListener(true);
                nameEt.setVisibility(View.GONE);
                emailEt.setVisibility(View.GONE);
                okBtn.setVisibility(View.VISIBLE);
                loadBtn.setVisibility(View.GONE);
                closeKeyboard();
            }
        }
    }

    private void closeKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getFocusedET().getWindowToken(), 0);
        }
    }

    private EditText getFocusedET() {
        if (nameEt.isFocused()) {
            return nameEt;
        } else if (emailEt.isFocused()) {
            return emailEt;
        }

        return null;
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }
}